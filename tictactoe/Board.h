//
//  Board.h
//  tictactoe
//
//  Created by Mateusz Nuckowski on 15/03/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol BoardDelegate <NSObject>

- (void)displayResult:(NSString *)result;

@end

@interface Board : NSObject

@property (weak, nonatomic) id <BoardDelegate> delegate;

- (instancetype)initWithArrayOfXAndO:(NSArray *)array;
- (void)checkTheBoard;

@end
