//
//  ViewController.m
//  tictactoe
//
//  Created by Mateusz Nuckowski on 15/03/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "ViewController.h"
#import "Board.h"

@interface ViewController () <BoardDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Board *board = [[Board alloc]initWithArrayOfXAndO:@[@"o",@"x",@"o",@"x",
                                                        @"x",@"o",@"o",@"x",
                                                        @"x",@"x",@"o",@"o",
                                                        @"x", @"o",@"x",@"o"]];
    board.delegate = self;
    
    [board checkTheBoard];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)displayResult:(NSString *)result
{
    UIAlertView *alerView = [[UIAlertView alloc] initWithTitle:@"Game result"
                                                       message:result
                                                      delegate:nil
                                             cancelButtonTitle:@"OK"
                                             otherButtonTitles:nil];
    
    [alerView show];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
