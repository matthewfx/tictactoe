//
//  Board.m
//  tictactoe
//
//  Created by Mateusz Nuckowski on 15/03/15.
//  Copyright (c) 2015 Mateusz Nuckowski. All rights reserved.
//

#import "Board.h"

@interface Board ()
@property (assign, nonatomic) NSInteger size;
@property (assign, nonatomic) NSInteger rows;
@property (assign, nonatomic) NSInteger columns;
@property (strong, nonatomic) NSArray *arrayOfXAndO;
@property (strong, nonatomic) NSMutableArray *results;
@end

@implementation Board

- (instancetype)initWithArrayOfXAndO:(NSArray *)array
{
    double arraySize = (double)[array count];
    if (sqrt(arraySize) != round(sqrt(arraySize))) {
        
        [self.delegate displayResult:@"Wrong number of fields for this board size!"];
        return nil;
    } else {
        self = [super init];
        self.size = [array count];
        self.rows = sqrt([array count]);
        self.columns = sqrt([array count]);
        self.results = [NSMutableArray arrayWithCapacity:(2 * self.rows + 2)];
        
        // fill results with 0s
        
        for (int i=0; i< (2 * self.rows + 2); i++) {
            self.results[i] = @(0);
        }
        
        self.arrayOfXAndO = array;
    }
    return self;
}

- (void)checkTheBoard
{
    NSInteger currentColumn = 0;
    NSInteger currentRow = 0;
    
    // itterate array of xs and os
    for (NSString *xo in self.arrayOfXAndO) {
        
        // check if value is valid
        if (![xo isEqualToString:@"x"] && ![xo isEqualToString:@"o"]) {
            [self.delegate displayResult:@"Wrong field value! Use x and o only!"];
            return;
        }
        
        
        NSInteger point = 0;
        
        // if X set point to +1 if O set to -1
        if ([xo isEqualToString:@"x"]) {
            point = 1;
        } else {
            point = -1;
        }
        
        
        // read current results from result array
        
        NSInteger currentRowValue = [self.results[currentRow] integerValue];
        NSInteger currentColumnValue = [self.results[currentColumn + self.rows] integerValue];
        
        NSInteger diagonalOneValue = [self.results[2 * self.rows] integerValue];
        NSInteger diagonalTwoValue = [self.results[2 * self.rows + 1] integerValue];
        
        // change results
        currentRowValue += point;
        currentColumnValue += point;
        
        if (currentRow == currentColumn) {
            diagonalOneValue += point;
        }
        
        if (self.rows - 1 - currentColumn == currentRow) {
            diagonalTwoValue += point;
        }
        
        // check if we have a victory
        
        if (currentRowValue == self.rows) {
            [self.delegate displayResult:[NSString stringWithFormat:@"Row of x in row number: %ld", currentRow + 1]];
            return;
        } else if (currentRowValue == -self.rows) {
            [self.delegate displayResult:[NSString stringWithFormat:@"Row of o in row number: %ld", currentRow + 1]];
            return;
        } else if (currentColumnValue == self.rows) {
            [self.delegate displayResult:[NSString stringWithFormat:@"Column of x in column number: %ld", currentColumn + 1]];
            return;
        } else if (currentColumnValue == - self.rows) {
            [self.delegate displayResult:[NSString stringWithFormat:@"Column of o in column number: %ld", currentColumn + 1]];
            return;
        } else if (diagonalOneValue == self.rows) {
            [self.delegate displayResult:[NSString stringWithFormat:@"First diagonal of x"]];
            return;
        } else if (diagonalOneValue == - self.rows) {
            [self.delegate displayResult:[NSString stringWithFormat:@"First diagonal of o"]];
            return;
        } else if (diagonalTwoValue == self.rows) {
            [self.delegate displayResult:[NSString stringWithFormat:@"Second diagonal of x"]];
            return;
        } else if (diagonalTwoValue == - self.rows) {
            [self.delegate displayResult:[NSString stringWithFormat:@"Second diagonal of o"]];
            return;
        }

        // write updated values to results array
        
        self.results[currentRow] = @(currentRowValue);
        self.results[currentColumn + self.rows] = @(currentColumnValue);
        self.results[2 * self.rows] = @(diagonalOneValue);
        self.results[2 * self.rows + 1] = @(diagonalTwoValue);
        
        // add columns
        
        //NSLog(@"POINT: %ld Row: %ld value: %ld, Column: %ld value: %ld, D1: %ld, D2: %ld", (long)point, (long)currentRow, (long)currentRowValue, (long)currentColumn, (long)currentColumnValue, (long)diagonalOneValue, (long)diagonalTwoValue);
        
        if (currentColumn == self.columns - 1) {
            currentColumn = 0;
            currentRow ++;
        } else {
            currentColumn ++;
        }
        
    }
    
    NSLog(@"%@",self.results);
    [self.delegate displayResult:[NSString stringWithFormat:@"No winner!"]];
}

@end
